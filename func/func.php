<?php
include 'db.php';

class func_db {
    public static $db_conn = null ;
  	public static $IParr=array('10.10.10.141','10.10.10.42','10.10.10.43','10.10.10.44','10.10.10.145');
  	public static $rabb_qname=array('useranx1','useranx2','useranx3','userpost1','userpost2');
  		// public static $IParr=array('10.224.56.64','10.224.56.65','10.224.56.69','10.224.56.70','10.224.56.71');
  	public static $dbpath="/home/pi/rfidGA/GA/jGA.jar.db";

    public function __construct() {
        self::$db_conn = Database::connect();
    }

    public static function getcurr_db() {


      return Database::getdb();


      }
     public static function get_piIPlist() {


      return   self::$IParr;


      }
       public static function get_pi123() {


      return   self::$rabb_qname;


      }

      public static function getdata($sql) {
    #("user_profile_member","idusers","6","iduser_profile_member","1")

            try {

                  $conn = self::$db_conn;
                  if (!$conn) {
                      $e = oci_error();
                      trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
                   }
                  $sql = $sql;
                  // echo "SQL:: ".$sql."<br>";
                   $stid = oci_parse($conn, $sql);
                   $res = oci_execute($stid);
                  // while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                  //     echo $row['TAG_SERIAL_NUM'] ."<br>";
                  // }
                  // $nrows = oci_fetch_all($stid, $res);
                  $nrows = oci_fetch_all($stid, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
                  // echo "$nrows rows fetched<br>\n";
                //  print_r($res);
                  $data =  $res;
                  #$data = $q->fetchall();
                 oci_free_statement($stid);

                  // oci_close($conn);

            return $data;
            Database::disconnect();
            }
          catch(PDOException $e)
            {
              return $e->getMessage();
            }

          $pdo = null;

       }

       public static function getdataJSON($url) {
     #("user_profile_member","idusers","6","iduser_profile_member","1")

             try {

               $json_url = $url;
               $json = file_get_contents($json_url);
               $data = json_decode($json, TRUE);

             return $data;
          //   Database::disconnect();
             }
           catch(PDOException $e)
             {
               return $e->getMessage();
             }

           $pdo = null;

        }
}
