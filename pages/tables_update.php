<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="refresh" content="420">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SPBO Teras</title>
    <link rel="icon" type="image/png" href="../img/teras.png"/>
    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="../dist/css/sidebar-toogle.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

  <div id="wrapper" class="active">
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                     <?php include '../func/sidetab_up2.php';?>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">


                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> RFID Tag <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->


            <!-- /.navbar-collapse -->
        </nav>
      <!-- Sidebar -->
            <!-- Sidebar -->
      <div id="sidebar-wrapper">
      <ul id="sidebar_menu" class="sidebar-nav">
             <?php include '../func/sidetab_up.php';?>
      </ul>
        <ul class="sidebar-nav" id="sidebar">
              <?php include '../func/sidetab.php';?>
        </ul>
      </div>

      <!-- Page content -->
      <div id="page-content-wrapper">
        <!-- Keep all page content within the page-content inset div! -->
        <div class="page-content inset">
          <div class="row">
              <div class="col-md-16">
                <div class="col-lg-12">
                     <p class="well lead">RFID TAG UPDATE</p>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <?php
            include "../func/func.php";
            $tab = "RFID_TAG_UPDATE";
            $sql = 'SELECT RUNNING_NUMBER, TAG_SERIAL_NUM,get_tag_status(rfid_tag_update.tag_status) as tag_stat,VEHICLE_PLATE_NUM,get_oper_type(rfid_tag_update.operation_type) as tag_oper, TAG_STATUS ,
                   UPDATED_TIMESTAMP FROM '.$tab .' order by RUNNING_NUMBER DESC';

 // $sql = 'SELECT rfid_tag_ack.serial_number,rfid_tag_update.vehicle_plate_num,get_oper_type(rfid_tag_update.operation_type)
      //       operation_type,get_tag_status(rfid_tag_update.tag_status) tag_status,rfid_tag_update.updated_timestamp
       //      FROM rfid_tag_ack LEFT JOIN rfid_tag_update on rfid_tag_ack.serial_number = rfid_tag_update.tag_serial_num
       //      ORDER BY rfid_tag_update.updated_timestamp desc';
            //  $dbh = Database::connect();
             $func = new func_db();
             $data = $func::getdata($sql);
             $db = $func::getcurr_db();
          // print_r($data);
            ?>

                <div class="col-lg-18">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Table : <?php  echo $tab.'  [Oracle]:<font color=red> :'.$db.'</font>'; ?>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover table-responsive" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>SERIAL_NUMBER</th>
                                        <th>VEHICLE_PLATE_NUM</th>

                                        <th>OPERATION_TYPE</th>

                                          <th>TAG_STATUS</th>
                                           <th>UPDATED_TIMESTAMP</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php
                                    $x=1;
                                foreach($data as $dat)
                                  {
                                  echo ' <tr class="odd gradeX">';
                                  echo '<td>'.$x.'</td>';
                                  echo '<td>'.$dat["TAG_SERIAL_NUM"].'</td>';
                                    echo '<td>'.$dat["VEHICLE_PLATE_NUM"].'</td>';
                                              echo '<td>'.$dat["TAG_OPER"].'</td>';
                                                echo '<td>'.$dat["TAG_STAT"].'</td>';
                                  //  echo '<td>'.$dat["UPDATED_TIMESTAMP"].'</td>';

                                     $norm_dt = date('Y-m-d H:i:s', strtotime($dat["UPDATED_TIMESTAMP"]));
                                      $date1 = $dat["UPDATED_TIMESTAMP"];
                                     echo '<td>'.$date1.'</td>';
                                    //   echo '<td>'.$dat["LEDGER_BALANCE"].'</td>';
                                    //       echo '<td>'.$dat["UPDATED_TIMESTAMP"].'</td>';
                                          // echo '<td>'.$dat["SYS_TIMESTAMP"].'</td>';
                                  echo '</tr>';
                                  $x+=1;
                                  }
                                  ?>

                                </tbody>
                            </table>
                            <!-- /.table-responsive -->

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>


          </div>
        </div>


    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>


    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });

    $(document).ready(function() {
        $('#datatab_rfid_tag_master').DataTable({
            responsive: true
        });
    });
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("active");
});
    </script>

</body>

</html>
